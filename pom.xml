<!--
  ~ Copyright 2013 OW2 Nanoko Project
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>org.nanoko</groupId>
    <artifactId>h-ubu</artifactId>
    <version>1.0.1-SNAPSHOT</version>
    <name>OW2 Nanoko ~ h-ubu</name>
    <packaging>wisdom</packaging>
    <description>H-Ubu is a simple component model for Javascript</description>

    <url>https://github.com/nanoko-project/h-ubu</url>
    <inceptionYear>2010</inceptionYear>

    <organization>
        <name>OW2</name>
        <url>http://ow2.org</url>
    </organization>

    <properties>
        <site.path>snapshot</site.path>
        <!-- Site Title -->
        <title>nanoko :: h-ubu</title>
        <github.global.server>github</github.global.server>
    </properties>

    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <mailingLists>
        <mailingList>
            <name>Nanoko</name>
            <subscribe>https://groups.google.com/group/nanoko</subscribe>
            <unsubscribe>https://groups.google.com/group/nanoko</unsubscribe>
            <post>nanoko@googlegroups.com</post>
            <archive>https://groups.google.com/group/nanoko</archive>
        </mailingList>
    </mailingLists>

    <issueManagement>
        <system>github</system>
        <url>https://github.com/nanoko-project/h-ubu/issues</url>
    </issueManagement>

    <dependencies>
        <dependency>
            <groupId>org.nanoko.libs</groupId>
            <artifactId>mootools-core</artifactId>
            <version>1.4.5</version>
            <type>js</type>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
    </distributionManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.wisdom-framework</groupId>
                <artifactId>wisdom-maven-plugin</artifactId>
                <version>0.8.0</version>
                <extensions>true</extensions>
                <configuration>
                    <packageWebJar>true</packageWebJar>
                    <disableDistributionPackaging>true</disableDistributionPackaging>
                    <googleClosureCompilationLevel>SIMPLE_OPTIMIZATIONS</googleClosureCompilationLevel>
                    <javascript>
                        <aggregations>
                            <aggregation>
                                <minification>false</minification>
                                <files>
                                    <file>js/Hubu-Root</file>
                                    <file>js/Utils</file>
                                    <file>js/AbstractComponent</file>
                                    <file>js/Hubu-Eventing-Extension</file>
                                    <file>js/Hubu-Binding-Extension</file>
                                    <file>js/Hubu-SOC-Mechanism</file>
                                    <file>js/Hubu-SOC-Extension</file>
                                    <file>js/Hub</file>
                                </files>
                                <output>hubu.js</output>
                            </aggregation>
                            <aggregation>
                                <minification>true</minification>
                                <files>
                                    <file>js/Hubu-Root</file>
                                    <file>js/Utils</file>
                                    <file>js/AbstractComponent</file>
                                    <file>js/Hubu-Eventing-Extension</file>
                                    <file>js/Hubu-Binding-Extension</file>
                                    <file>js/Hubu-SOC-Mechanism</file>
                                    <file>js/Hubu-SOC-Extension</file>
                                    <file>js/Hub</file>
                                </files>
                                <output>hubu.min.js</output>
                            </aggregation>
                        </aggregations>
                    </javascript>
                    <karmaPlugins>
                        <plugin>karma-coffee-preprocessor</plugin>
                        <plugin>karma-jasmine,0.1.5</plugin>
                        <plugin>karma-phantomjs-launcher,0.1.4</plugin>
                        <plugin>karma-junit-reporter</plugin>
                    </karmaPlugins>
                    <webjar>
                        <fileset>
                            <directory>${project.build.directory}/classes/assets</directory>
                            <excludes>
                                <exclude>**/js/*</exclude>
                            </excludes>
                        </fileset>
                    </webjar>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.sonatype.plugins</groupId>
                <artifactId>nexus-staging-maven-plugin</artifactId>
                <version>1.6.5</version>
                <extensions>true</extensions>
                <configuration>
                  <serverId>ossrh</serverId>
                  <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                  <autoReleaseAfterClose>true</autoReleaseAfterClose>
              </configuration>
          </plugin>

            <plugin>
                <artifactId>maven-site-plugin</artifactId>
                <version>3.4</version>
                <configuration>
                    <skipDeploy>true</skipDeploy>
                    <reportPlugins>
                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-project-info-reports-plugin</artifactId>
                            <version>2.8</version>
                            <configuration>
                                <dependencyDetailsEnabled>false</dependencyDetailsEnabled>
                                <dependencyLocationsEnabled>false</dependencyLocationsEnabled>
                            </configuration>
                        </plugin>
                        <plugin>
                            <groupId>org.nanoko.coffee-mill</groupId>
                            <artifactId>coffee-mill-maven-plugin</artifactId>
                            <version>1.1.4</version>
                            <configuration>
                                <jsdocIncludePrivate>true</jsdocIncludePrivate>
                            </configuration>
                        </plugin>
                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-surefire-report-plugin</artifactId>
                            <version>2.18.1</version>
                        </plugin>
                        <plugin>
                            <groupId>com.phasebash.jsdoc</groupId>
                            <artifactId>jsdoc3-maven-plugin</artifactId>
                            <version>1.1.0</version>
                            <configuration>
                                <recursive>true</recursive>
                                <directoryRoots>
                                    <directoryRoot>${basedir}/target/classes/assets/js</directoryRoot>
                                </directoryRoots>
                            </configuration>
                        </plugin>
                    </reportPlugins>
                </configuration>
                <dependencies>
                    <dependency>
                       <groupId>net.ju-n.maven.doxia</groupId>
                       <artifactId>doxia-module-markdown</artifactId>
                       <version>1.0.0</version>
                    </dependency>
                    <dependency>
                       <groupId>org.pegdown</groupId>
                       <artifactId>pegdown</artifactId>
                       <version>1.5.0</version>
                    </dependency>
                </dependencies>
            </plugin>

            <plugin>
                <!-- Deploy the web site -->
                <groupId>com.github.github</groupId>
                <artifactId>site-maven-plugin</artifactId>
                <version>0.11</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>site</goal>
                        </goals>
                        <phase>site-deploy</phase>
                        <configuration>
                            <message>Building site for ${project.version}</message>
                            <path>${site.path}</path>
                            <merge>true</merge>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <scm>
        <url>https://github.com/nanoko-project/h-ubu</url>
        <connection>scm:git:git@github.com:nanoko-project/h-ubu.git</connection>
        <developerConnection>scm:git:git@github.com:nanoko-project/h-ubu.git</developerConnection>
    </scm>

    <profiles>
        <profile>
            <id>release</id>
            <properties>
                <site.path>release</site.path>
            </properties>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <repositories>
        <repository>
            <id>oss-snapshot</id>
            <name>oss-snapshot</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>oss-snapshot</id>
            <name>oss-snapshot</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
        </pluginRepository>
    </pluginRepositories>
</project>

module.exports = function(config) {
    config.set({
        basePath: "${basedir}",
        frameworks: ['jasmine'],
        preprocessors: {
            '**/*.coffee': ['coffee']
        },
        files: [
            '${project.build.outputDirectory}/assets/hubu.js', //hubu!
            'src/test/coffee/*.coffee', //some component use for test
            'target/test-classes/*.js', //test dep.
            'src/test/js/*.js' //the actual test
        ],
        exclude: ['src/test/javascript/karma.conf.js'],
        port: 9876,
        logLevel: config.LOG_INFO,
        browsers: ['PhantomJS'],
        singleRun: true,
        plugins: [
            'karma-coffee-preprocessor',
            'karma-jasmine',
            'karma-phantomjs-launcher',
            'karma-junit-reporter'
        ],
        reporters:['progress', 'junit'],
        junitReporter: {
            outputFile: 'target/surefire-reports/karma-test-results.xml',
            suite: ''
        }
    });
};

scope = @

describe "Extensions Mechanism Test Suite", ->

  testHub = new scope.HUBU.Hub()

  comp =
    getComponentName: -> return "testComponent"
    configure: ->
    start: ->
    stop: ->

  startSpy = null
  stopSpy = null
  resetSpy = null
  registerSpy = null
  unregisterSpy = null

  beforeEach ->
    testHub.reset()

    startSpy = jasmine.createSpy "start"
    stopSpy = jasmine.createSpy "stop"
    resetSpy = jasmine.createSpy "reset"
    registerSpy = jasmine.createSpy "registerComponent"
    unregisterSpy = jasmine.createSpy "unregisterComponent"

    scope.getHubuExtensions().test = class TestExtension
      start : startSpy
      stop : stopSpy
      reset : resetSpy
      registerComponent : registerSpy
      unregisterComponent : unregisterSpy
    return


  it "should call start on registered extensions", ->
    testHub.start()

    expect(startSpy).toHaveBeenCalled()
    return

  it "should call stop on registered extensions", ->
    testHub.start()
    testHub.stop()

    expect(stopSpy).toHaveBeenCalled()
    return

  it "should call reset on registered extensions", ->
    testHub.start()
    testHub.reset()

    expect(resetSpy).toHaveBeenCalled()
    return

  it "should call registerComponent on registered extensions", ->
    testHub.registerComponent(comp)

    expect(registerSpy).toHaveBeenCalled()
    return

  it "should call unregisterComponent on registered extensions", ->
    testHub.registerComponent(comp)
    testHub.unregisterComponent(comp)

    expect(unregisterSpy).toHaveBeenCalled()
    return

  return